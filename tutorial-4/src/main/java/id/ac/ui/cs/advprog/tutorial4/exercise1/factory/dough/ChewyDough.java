package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ChewyDough implements Dough {
	
    public String toString() {
        return "Adonan Pizza yang Chewy";
    }
}
