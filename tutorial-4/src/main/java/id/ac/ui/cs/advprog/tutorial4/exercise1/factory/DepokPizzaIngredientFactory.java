package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new ChewyDough();
    }

    public Sauce createSauce() {
        return new JalapenoSauce();
    }
    
    public Sauce cheeseSauce() {
        return new CheeseSauce();
    }

    public Cheese createCheese() {
        return new KejuCheddar();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Zucchini(), new Mushroom(), new Onion(), new Eggplant(), new BlackOlives()};
        return veggies;
    }

    public Clams createClam() {
        return new KerangHijau();
    }
}
