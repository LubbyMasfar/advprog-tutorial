package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?

    // Eager initialization
	// private static Singleton instance = new Singleton();

	// Lazy initialization
	 private static Singleton instance;

    //private constructor
	private Singleton(){}

    public static Singleton getInstance() {
        // TODO Implement me!

        // Lazy initialization
    	if (instance == null) { // if there is no instance
    		instance = new Singleton(); // create new
    	}
        
        // Eager initialization no if conditional
    	// directly return instance
        return instance;
    }
}
