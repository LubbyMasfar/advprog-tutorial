package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class KejuCheddar implements Cheese {

    public String toString() {
        return "Parutan Keju Cheddar";
    }
}
