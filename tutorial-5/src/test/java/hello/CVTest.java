/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = VisitorController.class)
public class CVTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void titleCVTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("This is My CV")));
    }

    @Test
    public void nameTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Name:")));
    }


    @Test
    public void birthDateBirthplaceTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Birthdate & Birthplace:")));
    }

    @Test
    public void addressTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Address:")));
    }


    @Test
    public void educationTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Education:")));
    }

    @Test
    public void aboutMeTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("About Me")));
    }

    @Test
    public void visitorAccess() throws Exception {
        mockMvc.perform(get("/visitor").param("name", "John"))
                .andExpect(content().string(containsString("John, I hope you interested to hire me")));
    }


    /* @Test
    public void negtitleCVTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Not my CV")));
    }

     @Test
    public void negnameTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Nama:")));
    }

    @Test
    public void negbirthDateBirthplaceTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Tempat & Tanggal Lahir:")));
    }

     @Test
    public void negaddressTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Alamat:")));
    }

    @Test
    public void negaboutMeTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Tentang Saya")));
    }

    @Test
    public void negeducationTest() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/cv.html"))
                .andExpect(content().string(containsString("Pendidikan:")));
    }

    @Test
    public void negvisitorAccess() throws Exception {
        mockMvc.perform(get("/visitor").param("name", "John"))
                .andExpect(content().string(containsString("Ronaldo, I hope you interested to hire me")));
    } */
}