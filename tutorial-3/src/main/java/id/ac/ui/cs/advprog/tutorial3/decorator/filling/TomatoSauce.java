package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Food {
    Food food;

    public TomatoSauce(Food food) {
        //TODO Implement
        this.food = food;
    }

    public String getDescription() {
        //TODO Implement
        return food.getDescription() + ", adding tomato sauce";
    }

    public double cost() {
        //TODO Implement
        return .20 + food.cost();
    }
}
