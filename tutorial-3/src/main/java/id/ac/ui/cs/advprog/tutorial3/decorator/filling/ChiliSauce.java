package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        //TODO Implement
        this.food = food;
    }

    public String getDescription() {
        //TODO Implement
        return food.getDescription() + ", adding chili sauce";
    }

    public double cost() {
        //TODO Implement
        return .30 + food.cost();
    }
}
