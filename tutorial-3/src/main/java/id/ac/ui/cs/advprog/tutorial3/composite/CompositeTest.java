package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.*;

public class CompositeTest {
    
    private static Company company;
    private static Ceo luffy;
    private static Cto zorro;
    private static BackendProgrammer franky;
    private static BackendProgrammer usopp;
    private static FrontendProgrammer nami;
    private static FrontendProgrammer robin;
    private static UiUxDesigner sanji;
    private static NetworkExpert brook;
    private static SecurityExpert chopper;

public static void main(String[] args) {
    // TODO Auto-generated method stub
    
    company = new Company();
    
    luffy = new Ceo("Luffy", 500000.00);
    company.addEmployee(luffy);

    zorro = new Cto("Zorro", 320000.00);
    company.addEmployee(zorro);

    franky = new BackendProgrammer("Franky", 94000.00);
    company.addEmployee(franky);

    usopp = new BackendProgrammer("Usopp", 200000.00);
    company.addEmployee(usopp);

    nami = new FrontendProgrammer("Nami",66000.00);
    company.addEmployee(nami);

    robin = new FrontendProgrammer("Robin", 130000.00);
    company.addEmployee(robin);

    sanji = new UiUxDesigner("sanji", 177000.00);
    company.addEmployee(sanji);

    brook = new NetworkExpert("Brook", 83000.00);
    company.addEmployee(brook);
    
    chopper = new SecurityExpert("Chopper", 10000.00);
    company.addEmployee(chopper);
    
    
    // Print Data All Employees
    System.out.println("All Company Employee");
    printAllEmployees();
    
    System.out.println();
    
    //Print Total Employee's Salary
    System.out.println("Total Company Employee's Salary");
    salaryTotal();
   
    }

    public static void printAllEmployees() {
        List<Employees> allEmployees = company.getAllEmployees();
        
        for(int i = 0; i < allEmployees.size(); i++) {
            System.out.println(allEmployees.get(i).getName() + " - " +allEmployees.get(i).getRole() + " || Salary: " +
                    allEmployees.get(i).getSalary() );
        }
    }
    
    public static void salaryTotal() {
        List<Employees> allEmployees = company.getAllEmployees();
        
        double totalSalary = 0;
        for(int i = 0; i < allEmployees.size(); i++) {
            totalSalary = totalSalary + allEmployees.get(i).getSalary();
        }
        
        System.out.println(totalSalary);
    }

}
