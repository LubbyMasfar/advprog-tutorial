package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.*;

public class NetworkExpert extends Employees {
    //TODO Implement
	public NetworkExpert(String name, double salary) {
        //TODO Implement
    	this.name = name;
    	this.salary = salary;
    	this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
		//TODO Implement
    	return salary;
    }
	
}
