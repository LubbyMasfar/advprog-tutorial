package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;


public class DecorateBread {
 
	public static void main(String args[]) {
		Food food = new CrustySandwich();
		food = new BeefMeat(food);
		System.out.println(food.getDescription() 
				+ " | Total: $" + food.cost());
		
		Food food2 = new NoCrustSandwich();
		food2 = new BeefMeat(food2);
		food2 = new Cheese(food2);
		food2 = new Tomato(food2);
		food2 = new ChiliSauce(food2);
		System.out.println(food2.getDescription() 
				+ " | Total: $" + food2.cost());

		Food food3 = new ThickBunBurger();
		food3 = new ChickenMeat(food3);
		food3 = new Cucumber(food3);
		food3 = new Tomato(food3);
		System.out.println(food3.getDescription() 
				+ " | Total: $" + food3.cost());

		Food food4 = new ThinBunBurger();
		food4 = new BeefMeat(food4);
		food4 = new Cheese(food4);
		food4 = new Cucumber(food4);
		food4 = new Tomato(food4);
		food4 = new Lettuce(food4);
		food4 = new ChiliSauce(food4);
		food4 = new TomatoSauce(food4);
		System.out.println(food4.getDescription() 
				+ " | Total: $" + food4.cost());
	}
}